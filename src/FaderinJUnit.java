import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class FaderinJUnit {

    @Test
    public void testAssertions() {
        //test data
        String str10 = new String("Faderin first JUnit test, working with JUnit methods");
        String str20 = new String("Faderin first JUnit test, working with JUnit methods");
        String str30 = null;
        String str40 = "Faderin first JUnit test, working with JUnit methods";
        String str50 = "Faderin first JUnit test, working with JUnit methods";

        int val01 = 25;
        int val02 = 30;

        String[] expectedArray = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        String[] resultArray = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        //This check that the two expected objects are actual and equal
        Assert.assertEquals(str10, str20);

        //This check that a condition is true
        Assert.assertTrue(val01 < val02);

        //This check that a condition is false
        Assert.assertFalse(val01 > val02);

        //This check that an object isn't null
        Assert.assertNotNull(str20);

        //This check that an object is null
        Assert.assertNull(str30);

        //This check if two object references point to the same object
        Assert.assertSame(str40, str50);

        //This check if two object references not point to the same object
        Assert.assertNotSame(str10, str30);

        //This verifies each items in the  arrays are equal in the right position.
        Assert.assertArrayEquals(expectedArray, resultArray);

        // This is Deprecated. Asserts that a condition is true
        Assert.assertThat(str10, equalTo(str10));

    }

}
